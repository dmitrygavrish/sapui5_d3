sap.ui.define([
  'sap/ui/core/mvc/Controller'
], function (Controller) {
  'use strict';
  
  return Controller.extend('d3app.App', {
    onInit: function () {
      this.getView().addStyleClass('sapUiSizeCompact');
    },
  
    onSelectNode: function (oEvent) {
      var data = oEvent.getParameters();
  
      sap.m.MessageToast.show(data.title);
    }
  });
});
