sap.ui.define([
  'sap/ui/core/UIComponent',
  'sap/ui/model/json/JSONModel'
], function (
  UIComponent,
  JSONModel
) {
  'use strict';

  return UIComponent.extend('d3app.Component', {
    metadata: {
      manifest: 'json',
    },

    init: function () {
      UIComponent.prototype.init.apply(this, arguments);
      
      this.initModel('diagram');
    },
    
    initModel: function (modelName, callback) {
      var model = new JSONModel('models/' + modelName + '.json');
  
      this.setModel(model, modelName);
      
      if (callback) {
        model.attachRequestCompleted(callback);
      }
    }
  });
});
