sap.ui.define([
  'sap/ui/core/Control',
  'd3app/lib/diagram/defs',
  'd3app/lib/diagram/calc',
  'd3app/lib/diagram/interact'
], function (
  Control,
  defs,
  calc,
  interact
) {
  return Control.extend('d3app.controls.Diagram', {
    metadata: {
      properties: {
        nodes: {type: 'object', defaultValue: []},
        edges: {type: 'object', defaultValue: []}
      },
      events: {
        selectNode: {}
      }
    },
    
    onAfterRendering: function () {
      this.insertDiagram();
    },
    
    insertDiagram: function () {
      if (this._diagram) {
        this._diagram.remove();
      }
      
      var container = d3.select('#' + this.getId());
      var diagram = this._diagram = container.append('svg');
      
      diagram.svgGroup = diagram.append('g').classed('diagram', true);
      diagram.defs = diagram.append('svg:defs');
      diagram.edgesGroup = diagram.svgGroup.append('g').classed('edgeGroup', true);
      diagram.nodesGroup = diagram.svgGroup.append('g').classed('nodeGroup', true);
      diagram.nodesData = this.getNodes();
      diagram.edgesData = this.getEdges().map(function (edge) {
        edge.source = diagram.nodesData.find(function (node) {
          return node.sId === edge.source;
        });
        edge.target = diagram.nodesData.find(function (node) {
          return node.sId === edge.target;
        });
        
        return edge;
      });
  
      defs.addMarker(diagram.defs, 'arrowhead');
      
      diagram.call(interact.zoom(diagram)).on("dblclick.zoom", null);
      
      this.drawDiagram();
    },
    
    drawDiagram: function () {
      this.drawNodes();
      this.drawEdges();
    },
    
    drawNodes: function () {
      var self = this;
      var diagram = this._diagram;
      
      var nodes = diagram.nodesGroup.selectAll('.node')
        .data(diagram.nodesData, function (data) {
          return data.sId;
        });
      
      var node = nodes.enter().append('g').classed('node', true)
        .attr('transform', function(d) {
          return 'translate(' + d.x + ',' + d.y + ')';
        })
        .style('cursor', 'pointer')
        .on('click', function() {
          var previousNode = d3.select('.selected');
          var currentNode = d3.select(this);
          
          previousNode.classed('selected', false);
          currentNode.classed('selected', true);
          
          self.fireSelectNode(currentNode.datum());
        })
        .call(interact.dragNode(this.drawEdges.bind(this)));
      
      node.append("rect").classed('nodeRect', true);
      node.append("text").classed('nodeTitle', true)
        .html(function(data) {
          return data.title;
        });
      
      node.merge(nodes);
      nodes.exit().remove();
    },
    
    drawEdges: function () {
      var diagram = this._diagram;
      
      var edges = diagram.edgesGroup.selectAll('.edge')
        .data(diagram.edgesData, function(data) {
          return String(data.source.sId) + "+" + String(data.target.sId);
        })
        .attr("d", function(data) {
          return calc.calcPath(data);
        }.bind(this));
      
      
      var edge = edges.enter().append('path').classed('edge', true)
        .attr("d", function(data) {
          return calc.calcPath(data);
        }.bind(this))
        .style("marker-end", 'url(#arrowhead)');
  
      edge.merge(edges);
      edges.exit().remove();
    },
    
    dragNode: function () {
      var self = this;
      
      return d3.drag()
        .on("start", function() {
        })
        .on("drag", function(d) {
          var dragged = d3.select(this);
          
          d.x += d3.event.dx;
          d.y += d3.event.dy;
          
          dragged.attr("transform", "translate(" + d.x + "," + d.y + ")");
          self.drawEdges();
        })
        .on("end", function() {
        });
    },
  
    renderer: function (oRM, oControl) {
      oRM.write('<div');
      oRM.writeControlData(oControl);
      oRM.addClass('diagramContainer');
      oRM.writeClasses();
      oRM.write('>');
      oRM.write('</div>');
    }
  });
}, true);