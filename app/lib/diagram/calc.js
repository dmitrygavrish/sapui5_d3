sap.ui.define([], function () {
  return {
    measures: {
      functionWidth: 200,
      functionHeight: 100
    },
    
    calcPath: function (data) {
      var coords = this.calcVectorCoords(data);
  
      return "M" + coords.source.x + "," + coords.source.y
        + "L" + coords.target.x + "," + coords.target.y;
    },
  
    calcCoordsAdjustments: function (scales) {
      var xAxisK, yAxisK, x, y;
  
      // bordering to 40 deg will give delay for arrows on corners
      xAxisK = this.measures.functionWidth / 2 / 40;
      yAxisK = this.measures.functionHeight / 2 / 40;
  
      if (scales.tDegree < 45) {
        x = scales.tDegree <= 40 ? scales.tDegree * xAxisK : 40 * xAxisK;
        y = this.measures.functionHeight / 2;
      } else {
        x = this.measures.functionWidth / 2;
        y = scales.sDegree <= 40 ? scales.sDegree * yAxisK : 40 * yAxisK;
      }
  
      return {x: x, y: y};
    },
  
    calcVectorCoords: function (data) {
      var xS = Number(data.source.x);
      var yS = Number(data.source.y);
      var xT = Number(data.target.x);
      var yT = Number(data.target.y);
      var standardScales = {
        xDiff: Math.abs(xS - xT),
        yDiff: Math.abs(yS - yT)
      };
      
      standardScales.vectorLength = Math.sqrt(
        Math.pow(standardScales.xDiff, 2) + Math.pow(standardScales.yDiff, 2)
      );
      
      standardScales.sRadians = Math.asin(standardScales.yDiff / standardScales.vectorLength);
      standardScales.sDegree = standardScales.sRadians * 180 / Math.PI;
      standardScales.tDegree = 90 - standardScales.sDegree;
  
      var coordsAdj = this.calcCoordsAdjustments(standardScales);
      var xTargetAdjustment = xS > xT ? coordsAdj.x : -coordsAdj.x;
      var yTargetAdjustment = yS > yT ? coordsAdj.y : -coordsAdj.y;
  
      return {
        source: {
          x: xS,
          y: yS
        },
        target: {
          x: xT + xTargetAdjustment,
          y: yT + yTargetAdjustment
        }
      }
    }
  };
});