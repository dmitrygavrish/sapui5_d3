sap.ui.define([], function () {
  return {
    addMarker: function (defs, name) {
      defs.append("svg:marker")
        .attr("id", name)
        .attr("viewBox", "0 -5 15 10")
        .attr("refX", 13)
        .attr("markerWidth", 10)
        .attr("markerHeight", 10)
        .attr("orient", "auto")
        .append("svg:path")
        .attr("d", "M0,-5L15,0L0,5L2,0L0,-5");
    }
  };
});