sap.ui.define([], function () {
  return {
    dragNode: function (onDrag) {
      return d3.drag()
        .on("start", function() {
        })
        .on("drag", function(d) {
          var dragged = d3.select(this);
      
          d.x += d3.event.dx;
          d.y += d3.event.dy;
      
          dragged.attr("transform", "translate(" + d.x + "," + d.y + ")");
          onDrag();
        })
        .on("end", function() {
        });
    },
    
    zoom: function (diagram) {
      var scaleExtent = [0.5, 2.5];
      var translateExtent = [[-4000, -2500], [4000, 2500]];
      
      return d3.zoom()
        .scaleExtent(scaleExtent)
        .translateExtent(translateExtent)
        .on("zoom", function() {
          var transform = d3.zoomTransform(this);
    
          if (d3.event.sourceEvent && d3.event.sourceEvent.srcElement === diagram.node()) {
            // transform.toString() converts by default to "translate(x, y) scale(k)"
            diagram.svgGroup.attr("transform", transform);
          }
        });
    }
  };
});