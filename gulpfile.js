'use strict';

const gulp = require('gulp');
const browserSync = require('browser-sync').create();

gulp.task('serve', () => {
  browserSync.init({
    open: true,
    server: './app'
  });
});

gulp.task('reload', done => {
  browserSync.reload();
  done();
});

gulp.task('default', gulp.series(
  'serve'
));

gulp.watch('./app/**/*.*', gulp.series(
  'reload'
));
